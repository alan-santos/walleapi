package com.wallet.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.wallet.entity.User;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public class UserRepositoryTest {

	private static final String EMAIL = "email@teste.com";
	
	@Autowired
	private UserRepository repository;
	
	@BeforeEach
	public void setup() {
		User u = new User();
		u.setName("Test");
		u.setPassword("12345");
		u.setEmail(EMAIL);
		
		repository.save(u);
		
	}
	
	@AfterEach
	public void tearDown() {
		repository.deleteAll();
	}
	
	@Test
	public void testSave() {
		User u = new User();
		u.setName("Test");
		u.setPassword("12345");
		u.setEmail("teste@test.com.br");
		
		User response = repository.save(u);
		
		assertNotNull(response);
	}
	
	@Test
	public void findByEmail(){
		//Optional: Idicando que pode existir ou não um usuario.
		Optional<User> response = repository.findByEmail(EMAIL);
		
		//1. assertrue: validar que ese usuario existe
		assertTrue(response.isPresent());
		
		//2. assertequals: confirmando que o usuario que foi retonado possuio o mesmo email do usuario que sa
		assertEquals(response.get().getEmail(), EMAIL);
	}
}
