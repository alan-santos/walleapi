package com.wallet.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wallet.dto.UserWalletDTO;
import com.wallet.entity.User;
import com.wallet.entity.UserWallet;
import com.wallet.entity.Wallet;
import com.wallet.response.Response;
import com.wallet.service.UserWalletService;

@RestController
@RequestMapping("/user-wallet")
public class UserWalletController {

	@Autowired
	private UserWalletService service;
	
	public ResponseEntity<Response<UserWalletDTO>> create (@Valid @RequestBody UserWalletDTO dto, BindingResult result){
		Response<UserWalletDTO> response = new Response<UserWalletDTO>();
		
		if (result.hasErrors()) {
			result.getAllErrors().forEach(r -> response.getErrors().add(r.getDefaultMessage()));
			
			return ResponseEntity.badRequest().body(response);
		}
		
		UserWallet userWallet = service.save(this.convertToEntityDto(dto));
		response.setData(this.convertDtoToEntity(userWallet));
		
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}
	
	public UserWallet convertToEntityDto(UserWalletDTO dto) {
		UserWallet uw = new UserWallet();
		
		User u = new User();
		u.setId(dto.getId());
		
		Wallet w = new Wallet();
		w.setId(dto.getId());
		
		uw.setId(dto.getId());
		uw.setUser(u);
		uw.setWallet(w);
		
		return uw;
	}
	
	public UserWalletDTO convertDtoToEntity (UserWallet uw) {
		UserWalletDTO dto = new UserWalletDTO();
		
		dto.setId(uw.getId());
		dto.setUser(uw.getUser().getId());
		dto.setWallet(uw.getWallet().getId());
		
		return dto;
	}
}
